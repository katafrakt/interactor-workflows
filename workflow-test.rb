require 'hanami/interactor'
require 'json'

class PrintHello
  include Hanami::Interactor

  expose :output

  def call(name)
    @output = name * 2
    puts "Hello, #{name}"
  end
end

class Fail
  include Hanami::Interactor

  def call(input)
    @output = input
    fail!
  end
end

class Workflow
  def initialize
    @steps = []
  end

  def add(interactor_class)
    @steps << interactor_class
  end

  def run(input)
    Runner.new(@steps, input).call
  end

  NullOutput = Class.new

  class Runner
    attr_reader :first_input

    def initialize(steps, first_input)
      @steps = steps
      @first_input = first_input
      @tracer = Tracer.new(first_input)
    end

    def call
      input_to_pass = first_input
      output = nil

      @steps.each do |step|
        result = step.new.call(input_to_pass)
        output = result.respond_to?(:output) ? result.output : NullOutput.new
        tracer.trace_step(step, input_to_pass, result.successful?, output)

        break if result.failure?
        input_to_pass = output.is_a?(NullOutput) ? input_to_pass : output
      end

      Result.new(output, tracer.return)
    end

    private

    attr_reader :tracer
  end

  class Result
    attr_reader :output, :trace

    def initialize(output, trace)
      @output = output
      @trace = trace
    end
  end

  class Tracer
    def initialize(first_input)
      build_trace!(first_input)
    end

    def trace_step(interactor, input, is_successful, output)
      payload = {
        class: interactor.to_s,
        success: is_successful,
        input: JSON.dump(input)
      }

      unless output.is_a?(NullOutput)
        payload[:output] = JSON.dump(output)
      end

      @trace[:steps] << payload
    end

    def return
      @trace
    end

    private

    def build_trace!(first_input)
      @trace = {
        input: first_input,
        steps: []
      }
    end
  end
end

workflow = Workflow.new
workflow.add(PrintHello)
workflow.add(PrintHello)
workflow.add(Fail)
workflow.add(PrintHello)

result = workflow.run('world')
p result.trace
